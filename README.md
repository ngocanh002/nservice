# Đề tài `KHẮC PHỤC SỰ CỐ CHUNG CU`

### Người thực hiện :Nguyễn Văn Ngọc

#### Lý do chọn đề tài
- Mong muốn cải thiện cuộc sống của mọi người tạo lên những điều tốt đẹp.
Ngày nay
Mất điện, mất nước
   Mất mang internet
   Hỏng đèn, điều hòa
   Hỏng các trang thiết bị trong căn hộ....
	   => Tự sửa chữa
	   => Gọi thợ sửa
 Không hiệu quả, mất thời gian.

### Vì vậy ứng dụng sinh ra để

1. Cập nhật sự cố trong căn hộ của cư dân VindHome
2. Cư dân không cần liên hệ với người sửa chữa
3. Không mất chi phí với những sự cố trông whitelist
4. Lợi ích cư dân được đặt lên hàng đầu
5. Tốc độ nhanh chóng
6. An toàn

# Service

Dự án này được tạo ra bởi [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.


## Cài đặt nodejs sau khi chạy

Chạy lệnh `npm install`

## Chuyển tới thư mục src/mock-api

Chạy lệnh `json-server --host host-name --watch db.json --router router.json`

## Máy chủ phát triển

Chạy lệnh `ng serve` cho máy chủ dev. Chuyển tới `http://localhost:4200/`. Ứng dụng sẽ tự động thay dổi file nếu bạn thay đổi


## Đăng nhập admin vơi tài khoản: `admin` và password: `admin`

![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2Flg.png?alt=media&token=0f526c01-87b6-4a31-87b3-7190d80dddc7)

## Sơ đồ chức năng và cách thức hoạt động
### CƯ DÂN

![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2FUntitled.png?alt=media&token=5a53a5b5-c3e1-4c37-83c2-0de7371eff29)

### QUẢN TRỊ VIÊN

![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2FUntitledd.png?alt=media&token=81c41ba2-e575-483e-81db-27470a1c97e0)

### NHÂN VIÊN

![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2FUntitledRR.png?alt=media&token=a61aad44-54e5-4888-87ca-2836616121c1)

## Một số màn hình điều khiển

![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2Fm2.png?alt=media&token=f2ed28c6-26eb-4c58-a5f4-eb2bb8731415)
-
![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2Fm1.png?alt=media&token=d7bbc9d0-b524-45d1-b911-3614152fd257)
-
![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2Fm5.png?alt=media&token=165bfce0-7c83-4715-8f86-65c26745ebe3)
-
![alt text](https://firebasestorage.googleapis.com/v0/b/ngservice-17295.appspot.com/o/images%2Fm6.png?alt=media&token=7d993d38-d219-4416-84e8-3267672beeee)


## cơ bản với Angular CLI

Chạy lệnh `ng generate component component-name` hoặc `ng g c component-name` để tạo ra những component mới. Bạn cũng có thể sử dụng `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Xây dựng

Chạy lệnh `ng build` để xây dựng dự án. Phần xây dựng sẽ được lưu trữ trong thư mục `dist /`. Sử dụng `--prod` cho bản dựng sản xuất.

## Chạy unit tests

Chạy `ng test` để thực thi unit test thông qua [Karma](https://karma-runner.github.io).

## Chạy thử nghiệm đầu cuối

Chạy `ng e2e` để thực thi end-to-end tests thông qua [Protractor](http://www.protractortest.org/).

## Tips

Để nhận thêm trợ giúp về Angular CLI sử dụng `ng help` hoặc đi tới [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
