import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AdminComponent } from './pages/admin-pages/admin/admin.component';
import { ApartmentMalfunctionComponent } from './pages/admin-pages/apartment-malfunction/apartment-malfunction.component';
import { AuthGuard } from './helpers/auth.guard';
import { IncidentNoticeComponent } from './pages/user-pages/incident-notice/incident-notice.component';
import { UserComponent } from './pages/user-pages/user/user.component';
import { UserManagementComponent } from './pages/admin-pages/user-management/user-management.component';
import { EmployeemanagermentComponent } from './pages/admin-pages/employeemanagerment/employeemanagerment.component';
import { UserHomeComponent } from './pages/user-pages/user-home/user-home.component';
import { UserFeedbackComponent } from './pages/user-pages/user-feedback/user-feedback.component';
import { AdminHomeComponent } from './pages/admin-pages/admin-home/admin-home.component';
import { UserFlowstatusComponent } from './pages/user-pages/user-flowstatus/user-flowstatus.component';
import { NotificationsComponent } from './pages/employee-pages/notifications/notifications.component';
import { EmployeeComponent } from './pages/employee-pages/employee/employee.component';
import { EmployeeProfileComponent } from './pages/employee-pages/employee-profile/employee-profile.component';
import { DetailNotificationComponent } from './pages/employee-pages/notifications/detail-notification/detail-notification.component';
import { IncidentManagementComponent } from './pages/employee-pages/incident-management/incident-management.component';
import { ListIncidentComponent } from './pages/user-pages/list-incident/list-incident.component';
import { FeedbackManageComponent } from './pages/admin-pages/feedback-manage/feedback-manage.component';
import { EditUserComponent } from './pages/admin-pages/user-management/edit-user/edit-user.component';
import { EditemployeeComponent } from './pages/admin-pages/employeemanagerment/editemployee/editemployee.component';
import { DetailIncidentComponent } from './pages/admin-pages/apartment-malfunction/detail-incident/detail-incident.component';
import { DetailFeedbackComponent } from './pages/admin-pages/feedback-manage/detail-feedback/detail-feedback.component';
import { CreateNotificationComponent } from './pages/admin-pages/manage-notification/create-notification/create-notification.component';
import { ManageNotificationComponent } from './pages/admin-pages/manage-notification/manage-notification.component';
import { EditNotificationComponent } from './pages/admin-pages/manage-notification/edit-notification/edit-notification.component';
import { DetailComponent } from './pages/admin-pages/manage-notification/detail/detail.component';
import { UserNotificationDetailComponent } from './pages/user-pages/user-notification-detail/user-notification-detail.component';
import { EmployeeNotificationComponent } from './pages/employee-pages/employee-notification/employee-notification.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  {
    path: 'user/dashboard', component: UserComponent, canActivate: [AuthGuard],
    children: [
      { path: 'incident-notice', component: IncidentNoticeComponent },
      { path: 'home', component: UserHomeComponent },
      { path: 'feedback', component: UserFeedbackComponent },
      { path: 'flow-status', component: UserFlowstatusComponent },
      { path: 'lits-incident', component: ListIncidentComponent },
      { path: 'notification/:id/detail', component: UserNotificationDetailComponent }
    ]
  },
  {
    path: 'admin/dashboard', component: AdminComponent, canActivate: [AuthGuard],
    children: [
      { path: 'home', component: AdminHomeComponent },
      { path: 'malfunction', component: ApartmentMalfunctionComponent },
      { path: 'user-management', component: UserManagementComponent },
      { path: 'employee-management', component: EmployeemanagermentComponent },
      { path: 'feedback-management', component: FeedbackManageComponent },
      { path: 'user-management/:id/edit', component: EditUserComponent },
      { path: 'employee-management/:id/edit', component: EditemployeeComponent },
      { path: 'malfunction/:id/detail', component: DetailIncidentComponent },
      { path: 'feedback-management/:id/detail', component: DetailFeedbackComponent },
      { path: 'create-notification', component: CreateNotificationComponent },
      { path: 'notification-management', component: ManageNotificationComponent },
      { path: 'notification-management/:id/edit', component: EditNotificationComponent },
      { path: 'notification-management/:id/detail', component: DetailComponent },
      { path: '**', component: NotFoundComponent },
    ]
  },
  {
    path: 'employee/dashboard', component: EmployeeComponent, canActivate: [AuthGuard],
    children: [
      { path: 'notifications', component: NotificationsComponent },
      { path: 'notifications/:id/detail', component: DetailNotificationComponent },
      { path: 'profile', component: EmployeeProfileComponent },
      { path: 'incident-management', component: IncidentManagementComponent },
      { path: 'notify/:id/detail', component: EmployeeNotificationComponent}
    ]
  },

  { path: '**', component: NotFoundComponent },
  // { path: '**', redirectTo: '/home' },
  // { path: '**', redirectTo: '/admin' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
