import { Injectable } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { ConfigUrlService } from '../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DialogemployeeService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  addEmpl(value) {
    return this.apiService.post(this.configUrlService.postEmployee, value);
  }
}
