import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Users } from '../../shared/models/users';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogemployeeService } from './dialogemployee.service';
declare var $: any;

@Component({
  selector: 'app-dialog-employee',
  templateUrl: './dialog-employee.component.html',
  styleUrls: ['./dialog-employee.component.css']
})
export class DialogEmployeeComponent implements OnInit {

  public employeeForm: FormGroup;
  public submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private dialogemployeeService: DialogemployeeService,
    public dialogRef: MatDialogRef<DialogEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Users
  ) { }

  ngOnInit() {
    this.validateFromEmployee();
  }
  validateFromEmployee() {
    this.employeeForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      role: 3,
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      token: 'fake-jwt-token2'
    });
  }
  // getter value form
  get firstname() { return this.employeeForm.get('firstname'); }
  get lastname() { return this.employeeForm.get('lastname'); }
  get username() { return this.employeeForm.get('username'); }
  get password() { return this.employeeForm.get('password'); }
  get email() { return this.employeeForm.get('email'); }
  get phone() { return this.employeeForm.get('phone'); }
  get address() { return this.employeeForm.get('address'); }


  addEmployee() {
    this.submitted = true;

    if (this.employeeForm.invalid) {
      return;
    } else {
      const rawData = this.employeeForm.value;
      this.dialogemployeeService.addEmpl(rawData).subscribe((data) => {
        this.notification();
      });
    }
  }
  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
