import { TestBed } from '@angular/core/testing';

import { DialogemployeeService } from './dialogemployee.service';

describe('DialogemployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DialogemployeeService = TestBed.get(DialogemployeeService);
    expect(service).toBeTruthy();
  });
});
