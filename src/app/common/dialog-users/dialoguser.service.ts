import { Injectable } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { ConfigUrlService } from '../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DialoguserService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService,
  ) { }

  addUser(value) {
    return this.apiService.post(this.configUrlService.postResident, value);
  }
}
