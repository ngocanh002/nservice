import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Users } from '../../shared/models/users';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialoguserService } from './dialoguser.service';
declare var $: any;
@Component({
  selector: 'app-dialog-users',
  templateUrl: './dialog-users.component.html',
  styleUrls: ['./dialog-users.component.css']
})
export class DialogUsersComponent implements OnInit {

  public userForm: FormGroup;
  public submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private dialoguserService: DialoguserService,
    public dialogRef: MatDialogRef<DialogUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Users
  ) { }


  ngOnInit() {
    this.validateFromUser();
  }

  validateFromUser() {
    this.userForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      role: 2,
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      token: 'fake-jwt-token2'
    });
  }
  // getter value form
  get firstname() { return this.userForm.get('firstname'); }
  get lastname() { return this.userForm.get('lastname'); }
  get username() { return this.userForm.get('username'); }
  get password() { return this.userForm.get('password'); }
  get email() { return this.userForm.get('email'); }
  get phone() { return this.userForm.get('phone'); }
  get address() { return this.userForm.get('address'); }


  addAcount() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    } else {
      const rawData = this.userForm.value;
      this.dialoguserService.addUser(rawData).subscribe((data) => {
        this.notification();
      });
    }
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
