import { TestBed } from '@angular/core/testing';

import { DialoguserService } from './dialoguser.service';

describe('DialoguserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DialoguserService = TestBed.get(DialoguserService);
    expect(service).toBeTruthy();
  });
});
