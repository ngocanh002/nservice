import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApartmentMalfunction } from 'src/app/shared/models/apartmentmalfunction';
import { FlowstatusService } from 'src/app/pages/user-pages/user-flowstatus/flowstatus.service';

@Component({
  selector: 'app-dialog-popup',
  templateUrl: './dialog-popup.component.html',
  styleUrls: ['./dialog-popup.component.css']
})
export class DialogPopupComponent implements OnInit {
  public datamalfunctions: ApartmentMalfunction;
  public progress = false;

  constructor(
    private flowstatusService: FlowstatusService,
    public dialogRef: MatDialogRef<DialogPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ApartmentMalfunction
  ) { console.log(data); }

  ngOnInit() {
    this.getIncidentById();
  }

  getIncidentById() {
    this.flowstatusService.getById(this.data.id).subscribe((data) => {
      this.datamalfunctions = data;
      const status = 3;
      this.datamalfunctions.status = status;
      console.log(this.datamalfunctions);
    });
  }

  save() {
    const id = this.datamalfunctions.id;
    const newObject = this.datamalfunctions;
    this.flowstatusService.updateNitificatio(id, newObject).subscribe((data) => {
      this.progress = true;
      console.log(data);
    });
    console.log('danh gia va xac nhan thanh cong');
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
