export class ApartmentMalfunction {
    id: number;
    types: string[];
    description: string;
    image: string;
    phone: string;
    address: string;
    username: string;
    status: number;
    date: string;
    userid: any;
    employeeid: any;
}
