export class Notification {
    id: number;
    title: string;
    status: number;
    content: string;
    date?: string;
}
