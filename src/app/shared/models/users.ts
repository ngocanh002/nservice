export class Users {
    id: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    role: string;
    token?: string;
    email: string;
    phone: string;
    address: string;
}


