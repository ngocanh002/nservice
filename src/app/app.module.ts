import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminFooterComponent } from './components/admin-component/admin-footer/admin-footer.component';
import { AdminHeaderComponent } from './components/admin-component/admin-header/admin-header.component';
import { AdminNavbarComponent } from './components/admin-component/admin-navbar/admin-navbar.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AdminSidebarComponent } from './components/admin-component/admin-sidebar/admin-sidebar.component';


// form
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// material
import { MaterialModule } from './material';
// service
import { ApiService } from './service/api.service';
import { ConfigUrlService } from './service/config-url.service';
import { AdminComponent } from './pages/admin-pages/admin/admin.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ApartmentMalfunctionComponent } from './pages/admin-pages/apartment-malfunction/apartment-malfunction.component';
import { AuthenticationService } from './service/authentication.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { HeaderInterceptorProvider } from './helpers/fake-backend';
import { ApiConfig } from './configs/api.config';
import { UserHeaderComponent } from './components/user-component/user-header/user-header.component';
import { UserNavbarComponent } from './components/user-component/user-navbar/user-navbar.component';
import { UserFooterComponent } from './components/user-component/user-footer/user-footer.component';
import { IncidentNoticeComponent } from './pages/user-pages/incident-notice/incident-notice.component';
import { UserComponent } from './pages/user-pages/user/user.component';
import { UserManagementComponent } from './pages/admin-pages/user-management/user-management.component';
import { EmployeemanagermentComponent } from './pages/admin-pages/employeemanagerment/employeemanagerment.component';
import { UserHomeComponent } from './pages/user-pages/user-home/user-home.component';
import { UserFeedbackComponent } from './pages/user-pages/user-feedback/user-feedback.component';
import { AdminHomeComponent } from './pages/admin-pages/admin-home/admin-home.component';
import { AdminhomeService } from './pages/admin-pages/admin-home/adminhome.service';
import { UserFlowstatusComponent } from './pages/user-pages/user-flowstatus/user-flowstatus.component';
import { ApartmentmalfunctionService } from './pages/admin-pages/apartment-malfunction/apartmentmalfunction.service';
import { NotificationsComponent } from './pages/employee-pages/notifications/notifications.component';
import { EmployeeComponent } from './pages/employee-pages/employee/employee.component';
import { EmployeeNavbarComponent } from './components/employee-component/employee-navbar/employee-navbar.component';
import { EmployeeFooterComponent } from './components/employee-component/employee-footer/employee-footer.component';
import { EmployeeProfileComponent } from './pages/employee-pages/employee-profile/employee-profile.component';
import { NotificationsService } from './pages/employee-pages/notifications/notifications.service';
import { DetailNotificationComponent } from './pages/employee-pages/notifications/detail-notification/detail-notification.component';
import { IncidentManagementComponent } from './pages/employee-pages/incident-management/incident-management.component';
import { UsermanagermentService } from './pages/admin-pages/user-management/usermanagerment.service';
import { DialogPopupComponent } from './common/dialog-popup/dialog-popup.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { ListIncidentComponent } from './pages/user-pages/list-incident/list-incident.component';
import { FeedbackManageComponent } from './pages/admin-pages/feedback-manage/feedback-manage.component';
import { DialogUsersComponent } from './common/dialog-users/dialog-users.component';
import { DialogEmployeeComponent } from './common/dialog-employee/dialog-employee.component';
import { EditUserComponent } from './pages/admin-pages/user-management/edit-user/edit-user.component';
import { EditemployeeComponent } from './pages/admin-pages/employeemanagerment/editemployee/editemployee.component';
import { DeleteemployeeComponent } from './pages/admin-pages/employeemanagerment/deleteemployee/deleteemployee.component';
import { DeleteIncidentComponent } from './pages/admin-pages/apartment-malfunction/delete-incident/delete-incident.component';
import { DetailIncidentComponent } from './pages/admin-pages/apartment-malfunction/detail-incident/detail-incident.component';
import { DetailFeedbackComponent } from './pages/admin-pages/feedback-manage/detail-feedback/detail-feedback.component';
import { DeletefeedbackComponent } from './pages/admin-pages/feedback-manage/deletefeedback/deletefeedback.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { CreateNotificationComponent } from './pages/admin-pages/manage-notification/create-notification/create-notification.component';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ManageNotificationComponent } from './pages/admin-pages/manage-notification/manage-notification.component';
import { EditNotificationComponent } from './pages/admin-pages/manage-notification/edit-notification/edit-notification.component';
import { DetailComponent } from './pages/admin-pages/manage-notification/detail/detail.component';
import { DeleteNotificationComponent } from './pages/admin-pages/manage-notification/delete-notification/delete-notification.component';
import { UserNotificationDetailComponent } from './pages/user-pages/user-notification-detail/user-notification-detail.component';
import { EmployeeNotificationComponent } from './pages/employee-pages/employee-notification/employee-notification.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminFooterComponent,
    AdminHeaderComponent,
    AdminNavbarComponent,
    LoginComponent,
    RegisterComponent,
    AdminSidebarComponent,
    AdminComponent,
    NotFoundComponent,
    ApartmentMalfunctionComponent,
    UserHeaderComponent,
    UserNavbarComponent,
    UserFooterComponent,
    IncidentNoticeComponent,
    UserComponent,
    UserManagementComponent,
    EmployeemanagermentComponent,
    UserHomeComponent,
    UserFeedbackComponent,
    AdminHomeComponent,
    UserFlowstatusComponent,
    NotificationsComponent,
    EmployeeComponent,
    EmployeeNavbarComponent,
    EmployeeFooterComponent,
    EmployeeProfileComponent,
    DetailNotificationComponent,
    IncidentManagementComponent,
    DialogPopupComponent,
    ListIncidentComponent,
    FeedbackManageComponent,
    DialogUsersComponent,
    DialogEmployeeComponent,
    EditUserComponent,
    EditemployeeComponent,
    DeleteemployeeComponent,
    DeleteIncidentComponent,
    DetailIncidentComponent,
    DetailFeedbackComponent,
    DeletefeedbackComponent,
    CreateNotificationComponent,
    ManageNotificationComponent,
    EditNotificationComponent,
    DetailComponent,
    DeleteNotificationComponent,
    UserNotificationDetailComponent,
    EmployeeNotificationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // add firemodel
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireModule,
    AngularFireStorageModule,
    // form
    FormsModule,
    ReactiveFormsModule,
    // material
    MaterialModule,
    // ckeditor
    CKEditorModule
  ],
  providers: [
    ApiConfig,
    ApiService,
    ConfigUrlService,
    AdminhomeService,
    ApartmentmalfunctionService,
    UsermanagermentService,
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    HeaderInterceptorProvider
  ],
  entryComponents: [
    DialogPopupComponent,
    DialogUsersComponent,
    DialogEmployeeComponent,
    DeleteemployeeComponent,
    DeleteIncidentComponent,
    DeletefeedbackComponent,
    DeleteNotificationComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
