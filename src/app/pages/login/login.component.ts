import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../service/authentication.service';
import { ROLE, USERS } from '../../../mock-api/Constants';
import { Subscription } from 'rxjs';
import { UsermanagermentService } from '../admin-pages/user-management/usermanagerment.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  public hide = true;
  public loginForm: FormGroup;
  public loading = false;
  // public submitted = false;
  public returnUrl: string;
  public returnUrlUser: string;
  public returnUrlEmployee: string;
  public error: string;
  public subscription: Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
  }

  ngOnInit() {
    this.formToLogin();

  }

  formToLogin() {
    this.loginForm = this.formBuilder.group({
      account: ['', Validators.required],
      password: ['', Validators.required],
    });

    // tslint:disable-next-line:no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/admin/dashboard/home';
    this.returnUrlUser = 'user/dashboard/home';
    this.returnUrlEmployee = 'employee/dashboard/notifications';
  }
  // getter for assecc controller
  // tslint:disable-next-line:no-unused-expression
  get f() { return this.loginForm.controls; }

  // // validate
  // validateFormLogin() {
  //   this.validateService.getErrorMessage();
  // }
  getErrorAccount() {
    return this.f.account.hasError('required') ? 'Bạn chưa nhập tài khoản' : '';
  }
  getErrorPassword() {
    return this.f.password.hasError('required') ? 'Bạn chưa nhập mật khẩu' : '';
  }
  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.subscription = this.authenticationService.login(this.f.account.value, this.f.password.value)
      .pipe(first())
      .subscribe(data => {
        const role = parseInt(localStorage.getItem('role'), 10);
        // if (role === ROLE.ADMIN) {
        //   this.router.navigate([this.returnUrl]);
        // } else if (role === ROLE.USER) {
        //   this.router.navigate([this.returnUrlUser]);
        // }
        switch (role) {
          case ROLE.ADMIN:
            this.router.navigate([this.returnUrl]);
            break;
          case ROLE.USER:
            this.router.navigate([this.returnUrlUser]);
            break;
          case ROLE.EMPLOYEE:
            this.router.navigate([this.returnUrlEmployee]);
            break;
          default:
            this.router.navigate(['/login']);
            break;
        }
      }, error => {
        this.error = error;
        this.loading = false;
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
