import { TestBed } from '@angular/core/testing';

import { EmplnotifyService } from './emplnotify.service';

describe('EmplnotifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmplnotifyService = TestBed.get(EmplnotifyService);
    expect(service).toBeTruthy();
  });
});
