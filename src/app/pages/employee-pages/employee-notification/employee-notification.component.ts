import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmplnotifyService } from './emplnotify.service';

@Component({
  selector: 'app-employee-notification',
  templateUrl: './employee-notification.component.html',
  styleUrls: ['./employee-notification.component.css']
})
export class EmployeeNotificationComponent implements OnInit {

  public dataNotification;
  constructor(
    private route: ActivatedRoute,
    private emplnotifyService: EmplnotifyService
  ) { }

  ngOnInit() {
    this.getNotificationById();
  }

  getNotificationById() {
    const id = this.route.paramMap.subscribe(data => {
      console.log(data.get('id'));
      // tslint:disable-next-line:no-shadowed-variable
      this.emplnotifyService.getNotification(data.get('id')).subscribe(data => {
        this.dataNotification = data;
      });
    });
  }
}
