import { Injectable } from '@angular/core';
import { ConfigUrlService } from '../../../service/config-url.service';
import { ApiService } from '../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class EmplnotifyService {
  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getNotification(id) {
    return this.apiService.get(this.configUrlService.get_notication_by_id + id);
  }

}
