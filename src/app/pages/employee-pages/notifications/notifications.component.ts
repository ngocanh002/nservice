import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { ApartmentMalfunction } from 'src/app/shared/models/apartmentmalfunction';
import { AuthenticationService } from '../../../service/authentication.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Users } from '../../../shared/models/users';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
  providers: [NotificationsService]
})
export class NotificationsComponent implements OnInit, OnDestroy {

  public apartmentMalfunctions: ApartmentMalfunction[];
  public currentEmployee;
  public date = Date.now();
  public formNitification: FormGroup;
  public submitted = false;
  public subscription: Subscription;
  public length;
  constructor(
    private notificationsService: NotificationsService,
    private authenticationService: AuthenticationService,
  ) {
    this.currentEmployee = this.authenticationService.currentUserValue;
    // console.log(this.currentEmployee);
  }

  ngOnInit() {
    this.getApartmentmalfunctions();
  }

  getApartmentmalfunctions() {
    this.notificationsService.getApartmentmalfunction().subscribe((data) => {
      this.apartmentMalfunctions = data;
      this.length = this.apartmentMalfunctions.length;
      // this.apartmentMalfunctions.forEach((status) => {
      //     this.hidenotification = status.status;
      //     console.log(this.hidenotification);
      // });
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
