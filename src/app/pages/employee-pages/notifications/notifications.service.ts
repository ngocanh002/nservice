import { Injectable } from '@angular/core';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';
import { ConfigUrlService } from '../../../service/config-url.service';
import { ApiConfig } from '../../../configs/api.config';
import { ApiService } from '../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  filter: any;

  constructor(
    private apartmentmalfunctionService: ApartmentmalfunctionService,
    private configUrlService: ConfigUrlService,
    private apiService: ApiService
  ) { }

  getApartmentmalfunction() {
    return this.apartmentmalfunctionService.getMalunction();
  }
}
