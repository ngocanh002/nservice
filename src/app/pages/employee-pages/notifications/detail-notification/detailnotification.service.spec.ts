import { TestBed } from '@angular/core/testing';

import { DetailnotificationService } from './detailnotification.service';

describe('DetailnotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailnotificationService = TestBed.get(DetailnotificationService);
    expect(service).toBeTruthy();
  });
});
