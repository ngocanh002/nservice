import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DetailnotificationService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getDetail(id: number) {
    return this.apiService.get(this.configUrlService.getDetailNotification + id);
  }

  updateNtification(id, value) {
    return this.apiService.put(this.configUrlService.putNotification + id, value);
  }
}
