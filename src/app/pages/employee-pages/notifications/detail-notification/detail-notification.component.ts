import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { DetailnotificationService } from './detailnotification.service';
import { ApartmentMalfunction } from 'src/app/shared/models/apartmentmalfunction';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Users } from '../../../../shared/models/users';
import { AuthenticationService } from '../../../../service/authentication.service';
declare var $: any;
@Component({
  selector: 'app-detail-notification',
  templateUrl: './detail-notification.component.html',
  styleUrls: ['./detail-notification.component.css'],
  providers: [DetailnotificationService]
})
export class DetailNotificationComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public apartmentMalfunction: ApartmentMalfunction;
  public statusMalfunction;
  public currentEmployee;
  public formNotification: FormGroup = new FormGroup({});
  public submitted = false;
  public value;
  public hideForm = false;
  public alertRoute = false;
  public employee: Users;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private detailnotificationService: DetailnotificationService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.infoEmployee();
    this.getDetailNotification();
    this.form();
    // this.setTypes();
  }

  getDetailNotification() {
    // tslint:disable-next-line:prefer-const
    let id = +this.route.snapshot.paramMap.get('id');
    let length;
    const currentEmployee = this.employee.id;
    const status = 2;

    this.subscription = this.detailnotificationService.getDetail(id).subscribe((data) => {
      this.apartmentMalfunction = data;
      this.statusMalfunction = data.status;
      length = this.apartmentMalfunction.types;
      const typeControl = this.formNotification.controls.types as FormArray;
      // tslint:disable-next-line: no-shadowed-variable
      length.map(data => {
        if (data !== undefined && data !== null) {
          typeControl.push(this.formBuilder.control(''));
        }
      });
      this.apartmentMalfunction.status = status;
      this.apartmentMalfunction.employeeid = currentEmployee;
      // typeControl.patchValue(types);
      this.formNotification.patchValue(this.apartmentMalfunction); // push datamapping into from
    });
  }

  gotoNotification() {
    this.router.navigate(['/employee/dashboard/notifications']);
  }

  form() {
    this.formNotification = this.formBuilder.group({
      username: [''],
      phone: [''],
      address: [''],
      description: [''],
      status: ['2'],
      image: [''],
      date: [''],
      userid: [''],
      employeeid: [''],
      types: this.formBuilder.array([]),
      apiId: 'v1' // mock id api
    });
  }

  // get user
  infoEmployee() {
    this.subscription = this.authenticationService.currentUser.subscribe(data => {
      this.employee = data;
    });
  }

  onSubmit() {
    this.submitted = true;
    const rawData = this.formNotification.getRawValue(); // take value
    if (this.formNotification.invalid) {
      return;
    } else {
      this.updateNotification(rawData);
    }

  }

  updateNotification(body) {
    // tslint:disable-next-line:prefer-const
    let id = +this.route.snapshot.paramMap.get('id');
    this.subscription = this.detailnotificationService.updateNtification(id, body).subscribe((data) => {
      this.notification();
    });
  }
  alert() {
    this.alertRoute = true;
    // this.countDown();
  }
  notification() {
    // $('#message').show(100).delay(100);
    // $('#message').hide(100);
    this.router.navigate(['employee/dashboard/incident-management']);
  }

  // countDown() {
  //   let seconds = 1;
  //   let secondow = document.querySelector('#countdown');
  //   (function countdown() {
  //     secondow.textContent = 'Trang sẽ chuyển sau ' + seconds + 'giây';
  //     if (seconds-- > 0) { setTimeout(countdown, 1000); }
  //   })();
  // }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
