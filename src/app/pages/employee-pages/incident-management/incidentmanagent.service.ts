import { Injectable } from '@angular/core';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';

@Injectable({
  providedIn: 'root'
})
export class IncidentmanagentService {

  constructor(private apartmentmalfunctionService: ApartmentmalfunctionService) { }

  getIncident() {
    return this.apartmentmalfunctionService.getMalunction();
  }

}
