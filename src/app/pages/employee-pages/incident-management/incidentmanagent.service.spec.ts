import { TestBed } from '@angular/core/testing';

import { IncidentmanagentService } from './incidentmanagent.service';

describe('IncidentmanagentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidentmanagentService = TestBed.get(IncidentmanagentService);
    expect(service).toBeTruthy();
  });
});
