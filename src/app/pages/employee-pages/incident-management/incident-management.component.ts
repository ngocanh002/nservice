import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApartmentMalfunction } from '../../../shared/models/apartmentmalfunction';
import { Users } from '../../../shared/models/users';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../service/authentication.service';
import { IncidentmanagentService } from './incidentmanagent.service';
declare var $: any;
@Component({
  selector: 'app-incident-management',
  templateUrl: './incident-management.component.html',
  styleUrls: ['./incident-management.component.css']
})
export class IncidentManagementComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public apartmentMalfunctions: ApartmentMalfunction[];
  public currentEmployee: Users;
  public length;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private incidentmanagentService: IncidentmanagentService
  ) { }

  ngOnInit() {
    this.infoEmployee();
    this.getDetailIncident();
  }
  getDetailIncident() {
    this.subscription = this.incidentmanagentService.getIncident().subscribe((data) => {
      this.apartmentMalfunctions = data;
      this.length = this.apartmentMalfunctions.length;
    });
  }
  // goback() {
  //   this.router.navigate(['/employee/dashboard/notifications']);
  // }
  // get user
  infoEmployee() {
    this.subscription = this.authenticationService.currentUser.subscribe(data => {
      this.currentEmployee = data.id || '';
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
