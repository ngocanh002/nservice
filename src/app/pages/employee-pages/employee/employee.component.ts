import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROLE } from '../../../../mock-api/Constants';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  public role = 0;
  constructor(private router: Router, ) { }

  ngOnInit() {
    this.role = parseInt(localStorage.getItem('role'), 10);
    if (this.role !== ROLE.EMPLOYEE) {
      this.router.navigate(['/login']);
    }
  }

}
