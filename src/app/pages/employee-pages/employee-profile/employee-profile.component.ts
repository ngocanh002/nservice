import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {
  public currentEmpl;
  public datas;
  public leng;
  constructor(
    private authenticationService: AuthenticationService,
    private apartmentmalfunctionService: ApartmentmalfunctionService,
  ) { }

  ngOnInit() {
    this.getCurrentEml();
    this.getIcident();
  }

  getCurrentEml() {
    this.authenticationService.currentUser.subscribe(data => this.currentEmpl = data);
  }
  getIcident() {
    this.apartmentmalfunctionService.getMalunction().subscribe((data) => {
      this.datas = data;
      const totalJobs = data.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.employeeid == this.currentEmpl.id && x.status == 3) {
          return x;
        }
      });
      this.leng = totalJobs.length;
    });
  }
}
