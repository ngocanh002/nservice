import { TestBed } from '@angular/core/testing';

import { EmplprofileService } from './emplprofile.service';

describe('EmplprofileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmplprofileService = TestBed.get(EmplprofileService);
    expect(service).toBeTruthy();
  });
});
