import { TestBed } from '@angular/core/testing';

import { UsernotificationService } from './usernotification.service';

describe('UsernotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsernotificationService = TestBed.get(UsernotificationService);
    expect(service).toBeTruthy();
  });
});
