import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNotificationDetailComponent } from './user-notification-detail.component';

describe('UserNotificationDetailComponent', () => {
  let component: UserNotificationDetailComponent;
  let fixture: ComponentFixture<UserNotificationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNotificationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNotificationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
