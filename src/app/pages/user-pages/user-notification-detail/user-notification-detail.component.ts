import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UsernotificationService } from './usernotification.service';

@Component({
  selector: 'app-user-notification-detail',
  templateUrl: './user-notification-detail.component.html',
  styleUrls: ['./user-notification-detail.component.css']
})
export class UserNotificationDetailComponent implements OnInit {
  public dataNotification;
  constructor(
    private route: ActivatedRoute,
    private usernotificationService: UsernotificationService
  ) { }

  ngOnInit() {
    this.getNotificationById();
  }

  getNotificationById() {
    const id = this.route.paramMap.subscribe(data => {
      console.log(data.get('id'));
      // tslint:disable-next-line:no-shadowed-variable
      this.usernotificationService.getNotification(data.get('id')).subscribe(data => {
        this.dataNotification = data;
      });
    });
  }
}
