import { Injectable } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  postFeedback(value) {
    return this.apiService.post(this.configUrlService.postFeedback, value);
  }
}
