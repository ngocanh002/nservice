import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../service/authentication.service';
import { Users } from 'src/app/shared/models/users';
import { Subscription } from 'rxjs';
import { FeedbackService } from './feedback.service';
declare var $: any;

@Component({
  selector: 'app-user-feedback',
  templateUrl: './user-feedback.component.html',
  styleUrls: ['./user-feedback.component.css']
})
export class UserFeedbackComponent implements OnInit, OnDestroy {

  public date = Date.now();
  public feedbackForm: FormGroup;
  public submitted = false;
  private user: Users;
  public subscription: Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private feedbackService: FeedbackService
  ) { }

  ngOnInit() {
    this.infoUser();
    this.formIncidentNotice();
    $('textarea').focus();
  }

  // focus() {
  //   this.content.focus;
  // }

  formIncidentNotice() {
    this.feedbackForm = this.formBuilder.group({
      userid: this.user.id,
      name: this.user.lastname + ' ' + this.user.firstname,
      address: this.user.address,
      email: ['', Validators.required],
      phone: ['', Validators.required],
      content: ['', [Validators.required, Validators.maxLength(500)]]
    });
  }

  // get user
  infoUser() {
    this.subscription = this.authenticationService.currentUser.subscribe(data => {
      this.user = data;
    });
  }
  // getter
  get email() { return this.feedbackForm.get('email').setValue(this.dataUser.email); }
  get phone() { return this.feedbackForm.get('phone').setValue(this.dataUser.phone); }
  get userid() { return this.feedbackForm.get('userid').setValue(this.dataUser.id); }
  get content() { return this.feedbackForm.get('content'); }
  get dataUser() { return this.user; }

  resetForm() {
    this.content.reset();
    this.submitted = false;
  }

  onSubmit() {
    this.submitted = true;
    if (this.feedbackForm.invalid) {
      return;
    } else {
      const rawData = this.feedbackForm.getRawValue();
      this.addFeedback(rawData);
    }
  }

  addFeedback(body) {
    this.feedbackService.postFeedback(body).subscribe((data) => {
      console.log(data);
      this.notification();
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
