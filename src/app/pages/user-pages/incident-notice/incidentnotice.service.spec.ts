import { TestBed } from '@angular/core/testing';

import { IncidentnoticeService } from './incidentnotice.service';

describe('IncidentnoticeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidentnoticeService = TestBed.get(IncidentnoticeService);
    expect(service).toBeTruthy();
  });
});
