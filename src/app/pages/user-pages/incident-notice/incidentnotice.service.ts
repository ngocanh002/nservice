import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../configs/api.config';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class IncidentnoticeService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService,
    private storage: AngularFireStorage
  ) { }


  addApartmentMalfunction(value) {
    return this.apiService.post(this.configUrlService.postApartmentMalfunction, value);
  }

  getWhileList() {
    return this.apiService.get(this.configUrlService.getWhileList);
  }

  updateImage(image: File) {
    // console.log(image)
    const filePath = 'images/' + image.name;
    const ref = this.storage.ref(filePath);
    ref.put(image);
  }
}
