import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentNoticeComponent } from './incident-notice.component';

describe('IncidentNoticeComponent', () => {
  let component: IncidentNoticeComponent;
  let fixture: ComponentFixture<IncidentNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
