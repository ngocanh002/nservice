import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { IncidentnoticeService } from './incidentnotice.service';
import { IncidentNotice } from './incidentnotice';
import { AuthenticationService } from '../../../service/authentication.service';
import { Users } from 'src/app/shared/models/users';
import { Subscription } from 'rxjs';
import { SelectList } from '../../../shared/models/selectlist';
declare var $: any;
@Component({
  selector: 'app-incident-notice',
  templateUrl: './incident-notice.component.html',
  styleUrls: ['./incident-notice.component.css'],
  providers: [IncidentnoticeService]
})
export class IncidentNoticeComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public date = new Date();
  public incidentNoticeform: FormGroup;
  public incidentNotice: IncidentNotice;
  public submitted = false;
  private user: Users;
  public selectLists: SelectList;
  public image: File;
  public imagePreview;
  public imageName;
  constructor(
    private formBuilder: FormBuilder,
    private incidentnoticeService: IncidentnoticeService,
    private authenticationService: AuthenticationService
  ) {

  }

  // getter
  set idUser(id) { this.id = id; }
  get username() { return this.incidentNoticeform.get('username').setValue(this.dataUser.username); }
  get phone() { return this.incidentNoticeform.get('phone').setValue(this.dataUser.phone); }
  get address() { return this.incidentNoticeform.get('address').setValue(this.dataUser.address); }
  get description() { return this.incidentNoticeform.get('description'); }
  get dataUser() { return this.user; }
  get types() {
    return this.incidentNoticeform.get('types') as FormArray;
  }

  // custum date
  public datestring = this.date.getDate() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getFullYear() + ' ' +
    this.date.getHours() + ':' + this.date.getMinutes();
  // id
  public id;

  ngOnInit() {
    this.infoUser();
    this.getid();
    this.formIncidentNotice();
    this.selectList();
  }
  // get user
  infoUser() {
    this.authenticationService.currentUser.subscribe(data => {
      this.user = data;
    });
  }
  getid() {
    this.id = this.user.id || '';
  }
  formIncidentNotice() {
    this.incidentNoticeform = this.formBuilder.group({
      username: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      status: '1',
      // image: this.imageName,
      date: this.datestring,
      userid: this.id,
      employeeid: -1,
      types: this.formBuilder.array([this.formBuilder.control('')
      ], [Validators.required]),
    });
  }
  // add form array
  addtRow() {
    this.types.push(this.formBuilder.control(''));
  }
  // delete form array
  deleteRow(index: number) {
    this.types.removeAt(index);
  }

  onSubmit() {
    this.submitted = true;
    if (this.incidentNoticeform.invalid) {
      return;
    } else {
      this.onAddApartmentMalfunction(this.incidentNoticeform);
      // this.incidentnoticeService.updateImage(this.image);
      this.resetForm();
    }
  }

  resetForm() {
    this.types.reset();
    this.description.reset();
    this.submitted = false;
  }

  onAddApartmentMalfunction(incidentNotice) {
    this.subscription = this.incidentnoticeService.addApartmentMalfunction(incidentNotice.value).subscribe(data => {
      // console.log(data);
      this.notification();
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

  selectList() {
    this.subscription = this.incidentnoticeService.getWhileList().subscribe((data) => {
      this.selectLists = data;
    });
  }

  // uploadImage(files: FileList) {
  //   this.imageName = files[0].name;
  //   console.log(this.imageName);
  //   this.image = files[0];
  //   console.log(this.image);
  //   const reader = new FileReader();
  //   reader.onload = () => {
  //     this.imagePreview = reader.result;
  //     console.log(this.imagePreview);
  //   };
  //   reader.readAsDataURL(files[0]);
  // }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
