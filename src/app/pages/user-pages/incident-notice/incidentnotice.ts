export class IncidentNotice {
    id: number;
    type: string[];
    description: string;
    image: string;
    address: string;
    status: number;
    phone: string;
    date: string;
    userid: any;
}
