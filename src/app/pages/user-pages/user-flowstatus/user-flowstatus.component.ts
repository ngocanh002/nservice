import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FlowstatusService } from './flowstatus.service';
import { Subscription } from 'rxjs';
import { ApartmentMalfunction } from 'src/app/shared/models/apartmentmalfunction';
import { AuthenticationService } from '../../../service/authentication.service';
import { EmployeemanagermantService } from '../../admin-pages/employeemanagerment/employeemanagermant.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogPopupComponent } from '../../../common/dialog-popup/dialog-popup.component';

@Component({
  selector: 'app-user-flowstatus',
  templateUrl: './user-flowstatus.component.html',
  styleUrls: ['./user-flowstatus.component.css'],
  providers: [FlowstatusService, EmployeemanagermantService]
})
export class UserFlowstatusComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public apartmentmalfunctions: ApartmentMalfunction[];
  public datamalfunctions: ApartmentMalfunction;
  public employees;
  private currentUser;
  constructor(
    private flowstatusService: FlowstatusService,
    private authenticationService: AuthenticationService,
    private employeemanagermantService: EmployeemanagermantService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getApartmentmalfunction();
    this.getCurrentUser();
    this.getEmployee();
  }

  getCurrentUser() {
    this.subscription = this.authenticationService.currentUser.subscribe(data => {
      this.currentUser = data.id;
    });
  }
  get idUser() {
    return this.currentUser;
  }
  getApartmentmalfunction() {
    this.subscription = this.flowstatusService.getApartmentmalfunction().subscribe((data) => {
      // console.log(data);
      this.apartmentmalfunctions = data;
    });
  }
  getEmployee() {
    this.subscription = this.employeemanagermantService.getEmployee().subscribe((data) => {
      this.employees = data;
    });
  }

  openDialog(id): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DialogPopupComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
        this.getApartmentmalfunction();
      }
      console.log('The dialog was closed');
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
