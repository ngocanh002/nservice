import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFlowstatusComponent } from './user-flowstatus.component';

describe('UserFlowstatusComponent', () => {
  let component: UserFlowstatusComponent;
  let fixture: ComponentFixture<UserFlowstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFlowstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFlowstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
