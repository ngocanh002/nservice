import { TestBed } from '@angular/core/testing';

import { FlowstatusService } from './flowstatus.service';

describe('FlowstatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowstatusService = TestBed.get(FlowstatusService);
    expect(service).toBeTruthy();
  });
});
