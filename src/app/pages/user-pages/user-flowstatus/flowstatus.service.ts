import { Injectable } from '@angular/core';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class FlowstatusService {

  constructor(
    private apartmentmalfunctionService: ApartmentmalfunctionService,
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getApartmentmalfunction() {
    return this.apartmentmalfunctionService.getMalunction();
  }

  getById(id) {
    return this.apartmentmalfunctionService.getById(id);
  }

  updateNitificatio(id, body) {
    return this.apiService.put(this.configUrlService.putNotification + id, body);
  }

}
