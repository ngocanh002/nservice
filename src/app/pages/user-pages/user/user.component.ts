import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROLE } from '../../../../mock-api/Constants';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  role = 0;
  constructor(private router: Router) { }

  ngOnInit() {
    this.role = parseInt(localStorage.getItem('role'), 10);
    if (this.role !== ROLE.USER) {
      this.router.navigate(['/login']);
    }
  }


}
