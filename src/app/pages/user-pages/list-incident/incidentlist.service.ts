import { Injectable } from '@angular/core';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';
import { ConfigUrlService } from '../../../service/config-url.service';
import { ApiService } from '../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class IncidentlistService {

  constructor(
    private configUrlService: ConfigUrlService,
    private apiService: ApiService
  ) { }

  getList() {
    return this.apiService.get(this.configUrlService.getListIncident);
  }
}
