import { Component, OnInit } from '@angular/core';
import { ApartmentmalfunctionService } from '../../admin-pages/apartment-malfunction/apartmentmalfunction.service';
import { IncidentlistService } from './incidentlist.service';
import { AuthenticationService } from '../../../service/authentication.service';

@Component({
  selector: 'app-list-incident',
  templateUrl: './list-incident.component.html',
  styleUrls: ['./list-incident.component.css']
})
export class ListIncidentComponent implements OnInit {
  public lists;
  public cerrentUser;
  constructor(
    private incidentlistService: IncidentlistService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.getCureenUser();
    this.getListIncident();
  }

  getCureenUser() {
    this.authenticationService.currentUser.subscribe(data => this.cerrentUser = data.id);
  }
  getListIncident() {
    this.incidentlistService.getList().subscribe((data) => {
      this.lists = data;
      console.log(this.lists);
    });
  }
}
