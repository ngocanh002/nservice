import { TestBed } from '@angular/core/testing';

import { IncidentlistService } from './incidentlist.service';

describe('IncidentlistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidentlistService = TestBed.get(IncidentlistService);
    expect(service).toBeTruthy();
  });
});
