import { Component, OnInit } from '@angular/core';
import { ROLE } from '../../../../mock-api/Constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public role = 0;
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.role = parseInt(localStorage.getItem('role'), 10);
    if (this.role !== ROLE.ADMIN) {
      this.router.navigate(['/login']);
    }
  }

}
