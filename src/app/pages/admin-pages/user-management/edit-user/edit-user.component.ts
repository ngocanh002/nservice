import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../../../shared/models/users';
import { EdituserService } from './edituser.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  public userForm: FormGroup;
  public submitted = false;
  public users: Users;
  constructor(
    private formBuilder: FormBuilder,
    private edituserService: EdituserService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getUserById();
    this.validateFromUser();
  }

  getUserById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.edituserService.getUsers(id).subscribe((data) => {
      this.users = data;
      console.log(this.users);
      this.userForm.patchValue(this.users);
    });
  }

  validateFromUser() {
    this.userForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      role: 2,
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      apiId: 'v1',
      token: 'fake-jwt-token2'
    });
  }
  // getter value form
  get firstname() { return this.userForm.get('firstname'); }
  get lastname() { return this.userForm.get('lastname'); }
  get username() { return this.userForm.get('username'); }
  get password() { return this.userForm.get('password'); }
  get email() { return this.userForm.get('email'); }
  get phone() { return this.userForm.get('phone'); }
  get address() { return this.userForm.get('address'); }


  editAcount() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    } else {
      const rawData = this.userForm.value;
      this.updateRecident(rawData);
    }
  }

  updateRecident(body) {
    const id = this.route.snapshot.paramMap.get('id');
    this.edituserService.updateUser(id, body).subscribe((data) => {
      this.notification();
      this.router.navigate(['admin/dashboard/user-management']);
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }
}
