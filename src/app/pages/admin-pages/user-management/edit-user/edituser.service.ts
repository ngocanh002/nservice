import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class EdituserService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getUsers(id) {
    return this.apiService.get(this.configUrlService.get_user_by_id + id);
  }

  updateUser(id, value) {
    return this.apiService.put(this.configUrlService.put_user + id, value);
  }
}
