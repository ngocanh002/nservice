import { Injectable } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class UsermanagermentService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService,
  ) { }

  getUser() {
    return this.apiService.get(this.configUrlService.getAllUser);
  }

  getUsers(id) {
    return this.apiService.get(this.configUrlService.get_user_by_id + id);
  }

}
