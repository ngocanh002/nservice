import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsermanagermentService } from './usermanagerment.service';
import { Resident } from './resident';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogUsersComponent } from 'src/app/common/dialog-users/dialog-users.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit, OnDestroy {
  public residents: Resident[] = [];
  public subscription: Subscription;
  public name;
  public searchisEmpty = false;
  public sumPages;
  public collection = [];
  public config: any;
  constructor(
    private usermanagermentService: UsermanagermentService,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getAllUser();
    this.getSumpage();
  }

  getAllUser() {

    this.route.queryParamMap.subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      const name = data.get('name');
      if (name) {
        this.usermanagermentService.getUser().subscribe((user) => {
          this.residents = user.filter(x => {
            // tslint:disable-next-line:prefer-const
            let result = x.firstname.toLowerCase().indexOf(name.toLowerCase()) !== -1;
            return result;
          });
          if (this.residents.length === 0) {
            this.searchisEmpty = true;
          } else {
            this.searchisEmpty = false;
          }
        });
      } else {
        this.usermanagermentService.getUser().subscribe((user) => {
          this.residents = user;
        });
      }
    });
  }

  openDialog(): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DialogUsersComponent, {
      width: '80%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
      }
      console.log('The dialog was closed');
    });
  }

  searchByName() {
    this.router.navigate(['/admin/dashboard/user-management'], { queryParams: { name: this.name ? this.name : '' } });
    this.name = '';
    if (this.name === '') {
      this.searchisEmpty = false;
    }
  }

  getSumpage() {
    this.config = {
      currentPage: 1,
      itemsPerPage: 2,
      totalItems: 0
    };
    this.route.queryParams.subscribe(
      // tslint:disable-next-line:no-string-literal
      params => this.config.currentPage = params['page'] ? params['page'] : 1);
    // tslint:disable-next-line: prefer-const
    let numberPage = 2;
    this.usermanagermentService.getUser().subscribe((data) => {
      // tslint:disable-next-line: prefer-const
      let leng = data.length;
      // tslint:disable-next-line:prefer-const
      let pages = leng / numberPage;
      this.sumPages = Math.ceil(pages);
      for (let i = 1; i <= this.sumPages; i++) {
        this.collection.push(i);
      }

    });

  }


  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
