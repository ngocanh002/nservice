import { TestBed } from '@angular/core/testing';

import { UsermanagermentService } from './usermanagerment.service';

describe('UsermanagermentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsermanagermentService = TestBed.get(UsermanagermentService);
    expect(service).toBeTruthy();
  });
});
