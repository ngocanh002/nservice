import { Injectable } from '@angular/core';
import { ConfigUrlService } from '../../../../service/config-url.service';
import { ApiService } from '../../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class DeletenotificationService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  deleteNt(id) {
    return this.apiService.delete(this.configUrlService.get_notication_by_id + id);
  }
}
