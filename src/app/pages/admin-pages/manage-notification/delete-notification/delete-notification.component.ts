import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeletenotificationService } from './deletenotification.service';
@Component({
  selector: 'app-delete-notification',
  templateUrl: './delete-notification.component.html',
  styleUrls: ['./delete-notification.component.css']
})
export class DeleteNotificationComponent implements OnInit {

  constructor(
    private router: Router,
    private deletenotificationService: DeletenotificationService,
    public dialogRef: MatDialogRef<DeleteNotificationComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {console.log(data.id);
   }

  ngOnInit() {
  }

  deleteNotification() {
    this.deletenotificationService.deleteNt(this.data.id).subscribe((data) => {
      this.router.navigate(['/admin/dashboard/notification-management']);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
