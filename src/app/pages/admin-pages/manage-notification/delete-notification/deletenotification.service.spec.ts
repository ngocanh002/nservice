import { TestBed } from '@angular/core/testing';

import { DeletenotificationService } from './deletenotification.service';

describe('DeletenotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeletenotificationService = TestBed.get(DeletenotificationService);
    expect(service).toBeTruthy();
  });
});
