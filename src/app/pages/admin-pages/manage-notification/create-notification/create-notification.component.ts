import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormBuilder, Validator, Validators } from '@angular/forms';
import { CreatenotificationService } from './createnotification.service';
declare var $: any;
@Component({
  selector: 'app-create-notification',
  templateUrl: './create-notification.component.html',
  styleUrls: ['./create-notification.component.css']
})
export class CreateNotificationComponent implements OnInit {
  public Editor = ClassicEditor;
  public notificationForm: FormGroup;
  public submitted = false;
  public selectLists;
  public date = new Date();
  constructor(
    private formBuilder: FormBuilder,
    private createnotificationService: CreatenotificationService
  ) { }
  // custum date
  public datestring = this.date.getDate() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getFullYear() + ' ' +
    this.date.getHours() + ':' + this.date.getMinutes();
  ngOnInit() {
    this.validationForm();
    this.selectLists = [
      { id: 1, name: 'Tất cả' },
      { id: 2, name: 'Cư dân' },
      { id: 3, name: 'Nhân viên' }
    ];
  }

  validationForm() {
    this.notificationForm = this.formBuilder.group({
      title: ['', Validators.required],
      status: ['', Validators.required],
      content: ['', Validators.required],
      date: this.datestring,
    });
  }
  // getter
  get title() {
    return this.notificationForm.get('title');
  }
  get status() {
    return this.notificationForm.get('status');
  }
  get content() {
    return this.notificationForm.get('content');
  }

  onSubmit() {
    this.submitted = true;
    if (this.notificationForm.invalid) {
      return;
    }
    this.addNotification(this.notificationForm.getRawValue());
  }

  addNotification(body) {
    this.createnotificationService.postNotification(body).subscribe(data => {
      this.notification();
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

}
