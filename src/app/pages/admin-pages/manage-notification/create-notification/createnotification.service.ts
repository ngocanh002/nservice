import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../../configs/api.config';
import { ConfigUrlService } from '../../../../service/config-url.service';
import { ApiService } from '../../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class CreatenotificationService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  postNotification(value) {
    return this.apiService.post(this.configUrlService.post_notification, value);
  }
}
