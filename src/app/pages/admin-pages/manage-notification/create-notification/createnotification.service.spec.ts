import { TestBed } from '@angular/core/testing';

import { CreatenotificationService } from './createnotification.service';

describe('CreatenotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreatenotificationService = TestBed.get(CreatenotificationService);
    expect(service).toBeTruthy();
  });
});
