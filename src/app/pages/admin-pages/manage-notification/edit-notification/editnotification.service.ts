import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class EditnotificationService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getNotificationById(id) {
    return this.apiService.get(this.configUrlService.get_notication_by_id + id);
  }

  putNotification(id, value) {
    return this.apiService.put(this.configUrlService.edit_notification + id, value);
  }

}
