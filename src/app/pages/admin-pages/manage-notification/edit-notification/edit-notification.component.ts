import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreatenotificationService } from '../create-notification/createnotification.service';
import { EditnotificationService } from './editnotification.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-edit-notification',
  templateUrl: './edit-notification.component.html',
  styleUrls: ['./edit-notification.component.css']
})
export class EditNotificationComponent implements OnInit {
  public Editor = ClassicEditor;
  public notificationFormEdit: FormGroup;
  public submitted = false;
  public selectLists;
  public dataNotification;
  constructor(
    private formBuilder: FormBuilder,
    private editnotificationService: EditnotificationService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.validationFormEdit();
    this.getNotificationById();
    this.selectLists = [
      { id: 1, name: 'Tất cả' },
      { id: 2, name: 'Cư dân' },
      { id: 3, name: 'Nhân viên' }
    ];
  }

  validationFormEdit() {
    this.notificationFormEdit = this.formBuilder.group({
      title: ['', Validators.required],
      status: ['', Validators.required],
      content: ['', Validators.required],
      apiId: 'v1',
      date: ''
    });
  }
  // getter
  get title() {
    return this.notificationFormEdit.get('title');
  }
  get status() {
    return this.notificationFormEdit.get('status');
  }
  get content() {
    return this.notificationFormEdit.get('content');
  }

  getNotificationById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.editnotificationService.getNotificationById(id).subscribe(data => {
      this.dataNotification = data;
      this.notificationFormEdit.patchValue(this.dataNotification);
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.notificationFormEdit.invalid) {
      return;
    }
    const id = this.route.snapshot.paramMap.get('id');
    this.editNotification(id, this.notificationFormEdit.getRawValue());
  }

  editNotification(id, body) {
    this.editnotificationService.putNotification(id, body).subscribe(data => {
      this.notification();

      this.router.navigate(['/admin/dashboard/notification-management']);
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }

}
