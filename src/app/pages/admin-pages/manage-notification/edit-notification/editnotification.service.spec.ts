import { TestBed } from '@angular/core/testing';

import { EditnotificationService } from './editnotification.service';

describe('EditnotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditnotificationService = TestBed.get(EditnotificationService);
    expect(service).toBeTruthy();
  });
});
