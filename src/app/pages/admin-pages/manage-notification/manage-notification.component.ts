import { Component, OnInit } from '@angular/core';
import { ManagenotificationService } from './managenotification.service';
import { Notification } from '../../../shared/models/notification';

@Component({
  selector: 'app-manage-notification',
  templateUrl: './manage-notification.component.html',
  styleUrls: ['./manage-notification.component.css']
})
export class ManageNotificationComponent implements OnInit {
  public notifications: Notification;
  constructor(
    private managenotificationService: ManagenotificationService,
  ) { }

  ngOnInit() {
    this.getAllNotification();
  }

  getAllNotification() {
    this.managenotificationService.getNotification().subscribe(data => {
      this.notifications = data;
    });
  }
}
