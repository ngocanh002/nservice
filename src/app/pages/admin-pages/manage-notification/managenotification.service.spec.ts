import { TestBed } from '@angular/core/testing';

import { ManagenotificationService } from './managenotification.service';

describe('ManagenotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManagenotificationService = TestBed.get(ManagenotificationService);
    expect(service).toBeTruthy();
  });
});
