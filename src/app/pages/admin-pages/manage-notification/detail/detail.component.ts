import { Component, OnInit } from '@angular/core';
import { DetailService } from './detail.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeleteNotificationComponent } from '../delete-notification/delete-notification.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  public dataNotification;
  constructor(
    private detailService: DetailService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    this.getNotificationById();
  }
  getNotificationById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.detailService.getNotification(id).subscribe(data => {
      this.dataNotification = data;
    });
  }

  deleteNotificationPopup(id): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DeleteNotificationComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
      }
    });
  }

}
