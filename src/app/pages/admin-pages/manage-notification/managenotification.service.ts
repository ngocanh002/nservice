import { Injectable } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class ManagenotificationService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getNotification() {
    return this.apiService.get(this.configUrlService.get_notification);
  }

}
