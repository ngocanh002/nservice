import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminhomeService } from './adminhome.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit, OnDestroy {
  public malfunction;
  public incidents;
  public employee;
  public resident;
  public feedback;
  public subscription: Subscription;
  constructor(
    private adminhomeService: AdminhomeService
  ) { }

  ngOnInit() {
    this.getLengthMalfunction();
    this.getLengEmployee();
    this.getResident();
    this.getFeedbackUser();
  }

  getLengthMalfunction() {
    this.subscription = this.adminhomeService.getLengthMalfunction().subscribe(data => {
      this.malfunction = data.length;
      this.incidents = data;
    });
  }

  getLengEmployee() {
    this.subscription = this.adminhomeService.getLengEmployee().subscribe(data => this.employee = data.length);
  }

  getResident() {
    this.subscription = this.adminhomeService.getResident().subscribe(data => this.resident = data.length);
  }

  getFeedbackUser() {
    this.adminhomeService.getFeedbacks().subscribe(data => this.feedback = data.length);
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
