import { Injectable } from '@angular/core';
import { ConfigUrlService } from '../../../service/config-url.service';
import { ApiService } from '../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminhomeService {
  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getLengthMalfunction() {
    return this.apiService.get(this.configUrlService.getAllApartmentMalfunction);
  }
  getLengEmployee() {
    return this.apiService.get(this.configUrlService.getEmployee);
  }
  getResident() {
    return this.apiService.get(this.configUrlService.getAllUser);
  }
  getFeedbacks() {
    return this.apiService.get(this.configUrlService.getFeedback);
  }
}
