import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DetaifeedbackService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getFeedback(id) {
    return this.apiService.get(this.configUrlService.get_feedback_by_id  + id);
  }
}
