import { TestBed } from '@angular/core/testing';

import { DetaifeedbackService } from './detaifeedback.service';

describe('DetaifeedbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetaifeedbackService = TestBed.get(DetaifeedbackService);
    expect(service).toBeTruthy();
  });
});
