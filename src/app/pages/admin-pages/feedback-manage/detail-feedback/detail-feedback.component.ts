import { Component, OnInit } from '@angular/core';
import { DetaifeedbackService } from './detaifeedback.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeletefeedbackComponent } from '../deletefeedback/deletefeedback.component';
@Component({
  selector: 'app-detail-feedback',
  templateUrl: './detail-feedback.component.html',
  styleUrls: ['./detail-feedback.component.css']
})
export class DetailFeedbackComponent implements OnInit {
  public feedback;
  constructor(
    private detaifeedbackService: DetaifeedbackService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getFeedbackByid();
  }

  getFeedbackByid() {
    const id = this.route.snapshot.paramMap.get('id');
    this.detaifeedbackService.getFeedback(id).subscribe(data => {
      this.feedback = data;
    });
  }

  deleteFeebackPopup(id): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DeletefeedbackComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
      }
    });
  }
}
