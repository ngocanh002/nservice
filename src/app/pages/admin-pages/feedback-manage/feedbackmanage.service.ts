import { Injectable } from '@angular/core';
import { AdminhomeService } from '../admin-home/adminhome.service';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackmanageService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  feedback() {
    return this.apiService.get(this.configUrlService.getFeedback);
  }
}
