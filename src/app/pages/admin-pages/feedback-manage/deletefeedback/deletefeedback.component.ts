import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeletefeebackService } from './deletefeeback.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deletefeedback',
  templateUrl: './deletefeedback.component.html',
  styleUrls: ['./deletefeedback.component.css']
})
export class DeletefeedbackComponent implements OnInit {

  constructor(
    private router: Router,
    private deletefeebackService: DeletefeebackService,
    public dialogRef: MatDialogRef<DeletefeedbackComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {console.log(data.id);
   }

  ngOnInit() {
  }

  deleteFeedback() {
    this.deletefeebackService.deletefb(this.data.id).subscribe((data) => {
      this.router.navigate(['/admin/dashboard/feedback-management']);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
