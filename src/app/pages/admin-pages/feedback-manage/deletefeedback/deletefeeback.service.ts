import { Injectable } from '@angular/core';
import { DetaifeedbackService } from '../detail-feedback/detaifeedback.service';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DeletefeebackService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  deletefb(id) {
    return this.apiService.delete(this.configUrlService.get_feedback_by_id + id);
  }
}
