import { TestBed } from '@angular/core/testing';

import { DeletefeebackService } from './deletefeeback.service';

describe('DeletefeebackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeletefeebackService = TestBed.get(DeletefeebackService);
    expect(service).toBeTruthy();
  });
});
