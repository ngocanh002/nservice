import { Component, OnInit } from '@angular/core';
import { FeedbackmanageService } from './feedbackmanage.service';
import { MatDialog } from '@angular/material/dialog';
import { DeletefeedbackComponent } from './deletefeedback/deletefeedback.component';

@Component({
  selector: 'app-feedback-manage',
  templateUrl: './feedback-manage.component.html',
  styleUrls: ['./feedback-manage.component.css']
})
export class FeedbackManageComponent implements OnInit {
  public feedbacks;
  constructor(private feedbackmanageService: FeedbackmanageService ) { }

  ngOnInit() {
    this.feedbackmanageService.feedback().subscribe(data => this.feedbacks = data);
  }

}
