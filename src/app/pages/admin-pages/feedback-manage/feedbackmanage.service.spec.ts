import { TestBed } from '@angular/core/testing';

import { FeedbackmanageService } from './feedbackmanage.service';

describe('FeedbackmanageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeedbackmanageService = TestBed.get(FeedbackmanageService);
    expect(service).toBeTruthy();
  });
});
