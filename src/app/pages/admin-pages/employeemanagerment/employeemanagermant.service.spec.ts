import { TestBed } from '@angular/core/testing';

import { EmployeemanagermantService } from './employeemanagermant.service';

describe('EmployeemanagermantService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeemanagermantService = TestBed.get(EmployeemanagermantService);
    expect(service).toBeTruthy();
  });
});
