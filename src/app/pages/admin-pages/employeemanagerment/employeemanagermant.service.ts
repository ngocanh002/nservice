import { Injectable } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeemanagermantService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getEmployee() {
    return this.apiService.get(this.configUrlService.getEmployee);
  }

}
