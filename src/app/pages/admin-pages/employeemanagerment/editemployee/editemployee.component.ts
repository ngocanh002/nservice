import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../../../shared/models/users';
import { ActivatedRoute, Router } from '@angular/router';
import { EditemplyeeService } from './editemplyee.service';
declare var $: any;
@Component({
  selector: 'app-editemployee',
  templateUrl: './editemployee.component.html',
  styleUrls: ['./editemployee.component.css']
})
export class EditemployeeComponent implements OnInit {

  public employeeForm: FormGroup;
  public submitted = false;
  public employee: Users;
  constructor(
    private formBuilder: FormBuilder,
    private editemplyeeService: EditemplyeeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getUserById();
    this.validateFromEmployee();
  }
  getUserById() {
    const id = this.route.snapshot.paramMap.get('id');
    this.editemplyeeService.getEmployee(id).subscribe((data) => {
      this.employee = data;
      console.log(this.employee);
      this.employeeForm.patchValue(this.employee);
    });
  }

  validateFromEmployee() {
    this.employeeForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      role: 3,
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      apiId: 'v1',
      token: 'fake-jwt-token2'
    });
  }
  // getter value form
  get firstname() { return this.employeeForm.get('firstname'); }
  get lastname() { return this.employeeForm.get('lastname'); }
  get username() { return this.employeeForm.get('username'); }
  get password() { return this.employeeForm.get('password'); }
  get email() { return this.employeeForm.get('email'); }
  get phone() { return this.employeeForm.get('phone'); }
  get address() { return this.employeeForm.get('address'); }

  editEmployee() {
    this.submitted = true;
    if (this.employeeForm.invalid) {
      return;
    } else {
      const rawData = this.employeeForm.value;
      this.updateEmployee(rawData);
    }
  }

  updateEmployee(body) {
    const id = this.route.snapshot.paramMap.get('id');
    this.editemplyeeService.updateEmployee(id, body).subscribe((data) => {
      this.notification();
      this.router.navigate(['admin/dashboard/employee-management']);
    });
  }

  notification() {
    $('#message').show(1000).delay(1000);
    $('#message').hide(1000);
  }
}
