import { TestBed } from '@angular/core/testing';

import { EditemplyeeService } from './editemplyee.service';

describe('EditemplyeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditemplyeeService = TestBed.get(EditemplyeeService);
    expect(service).toBeTruthy();
  });
});
