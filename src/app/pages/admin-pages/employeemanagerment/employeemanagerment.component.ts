import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeemanagermantService } from './employeemanagermant.service';
import { Employee } from './employee';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogEmployeeComponent } from '../../../common/dialog-employee/dialog-employee.component';
import { DeleteemployeeComponent } from './deleteemployee/deleteemployee.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employeemanagerment',
  templateUrl: './employeemanagerment.component.html',
  styleUrls: ['./employeemanagerment.component.css']
})
export class EmployeemanagermentComponent implements OnInit, OnDestroy {
  public employees: Employee[];
  public subscription: Subscription;
  public name;
  public searchisEmpty = false;
  constructor(
    public dialog: MatDialog,
    private employeemanagermantService: EmployeemanagermantService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getAllEmployee();
  }

  getAllEmployee() {

    this.route.queryParamMap.subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      const name = data.get('name');
      if (name) {
        this.subscription = this.employeemanagermantService.getEmployee().subscribe((user) => {
          this.employees = user.filter(x => {
            // tslint:disable-next-line:prefer-const
            let result = x.firstname.toLowerCase().indexOf(name.toLowerCase()) !== -1;
            return result;
          });
          if (this.employees.length === 0) {
            this.searchisEmpty = true;
          } else {
            this.searchisEmpty = false;
          }
        });
      } else {
        this.employeemanagermantService.getEmployee().subscribe((user) => {
          this.employees = user;
        });
      }
    });
  }

  searchByName() {
    this.router.navigate(['/admin/dashboard/employee-management'], { queryParams: { name: this.name } });
    this.name = '';
    if (this.name === '') {
      this.searchisEmpty = false;
    }
  }

  openDialog(): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DialogEmployeeComponent, {
      width: '80%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
      }
      console.log('The dialog was closed');
    });
  }

  deleteEmployeePopup(id): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DeleteemployeeComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
        this.getAllEmployee();
      }
      console.log('The dialog was closed');
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
