import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApartmentMalfunction } from 'src/app/shared/models/apartmentmalfunction';
import { FlowstatusService } from 'src/app/pages/user-pages/user-flowstatus/flowstatus.service';
import { DeleteemployeeService } from './deleteemployee.service';

@Component({
  selector: 'app-deleteemployee',
  templateUrl: './deleteemployee.component.html',
  styleUrls: ['./deleteemployee.component.css']
})
export class DeleteemployeeComponent implements OnInit {

  constructor(
    private deleteemployeeService: DeleteemployeeService,
    public dialogRef: MatDialogRef<DeleteemployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { console.log(data); }

  ngOnInit() {
  }

  deleteEmployee() {
    this.deleteemployeeService.deleteEmpl(this.data.id).subscribe((data) => {
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
