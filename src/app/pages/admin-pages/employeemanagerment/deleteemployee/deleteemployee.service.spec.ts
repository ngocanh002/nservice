import { TestBed } from '@angular/core/testing';

import { DeleteemployeeService } from './deleteemployee.service';

describe('DeleteemployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteemployeeService = TestBed.get(DeleteemployeeService);
    expect(service).toBeTruthy();
  });
});
