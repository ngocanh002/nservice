import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DeleteemployeeService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  deleteEmpl(id) {
    return this.apiService.delete(this.configUrlService.delete_employee + id);
  }
}
