import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeemanagermentComponent } from './employeemanagerment.component';

describe('EmployeemanagermentComponent', () => {
  let component: EmployeemanagermentComponent;
  let fixture: ComponentFixture<EmployeemanagermentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeemanagermentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeemanagermentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
