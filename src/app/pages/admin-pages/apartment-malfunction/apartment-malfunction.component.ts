import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ApartmentMalfunction } from '../../../shared/models/apartmentmalfunction';
import { ApartmentmalfunctionService } from './apartmentmalfunction.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DeleteIncidentComponent } from './delete-incident/delete-incident.component';

@Component({
  selector: 'app-apartment-malfunction',
  templateUrl: './apartment-malfunction.component.html',
  styleUrls: ['./apartment-malfunction.component.css']
})
export class ApartmentMalfunctionComponent implements OnInit, OnDestroy {
  public apartmentMalfunctions: ApartmentMalfunction[];
  public subscription: Subscription;
  public pending: any = [];
  public doing: any = [];
  public done: any = [];

  // public numberApartmentMalfunctions;
  // @Output() numberApartmentMalfunctions = new EventEmitter<any>();

  constructor(
    private apartmentmalfunctionService: ApartmentmalfunctionService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.subscription = this.apartmentmalfunctionService.getMalunction().subscribe((data) => {
      this.apartmentMalfunctions = data;

      this.pending = this.apartmentMalfunctions.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.status == 1) {
          return x;
        }
      });
      this.doing = this.apartmentMalfunctions.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.status == 2) {
          return x;
        }
      });

      this.done = this.apartmentMalfunctions.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.status == 3) {
          return x;
        }
      });

    });
  }

  deleteIcidentPopup(id): void {
    // this.getIncidentById(id);
    const dialogRef = this.dialog.open(DeleteIncidentComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('success');
        this.getAll();
      }
      console.log('The dialog was closed');
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
