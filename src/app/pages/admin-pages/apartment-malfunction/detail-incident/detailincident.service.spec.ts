import { TestBed } from '@angular/core/testing';

import { DetailincidentService } from './detailincident.service';

describe('DetailincidentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailincidentService = TestBed.get(DetailincidentService);
    expect(service).toBeTruthy();
  });
});
