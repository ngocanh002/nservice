import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../../configs/api.config';
import { ConfigUrlService } from '../../../../service/config-url.service';
import { ApartmentmalfunctionService } from '../apartmentmalfunction.service';
import { ApiService } from '../../../../service/api.service';

@Injectable({
  providedIn: 'root'
})
export class DetailincidentService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService,
    private apartmentmalfunctionService: ApartmentmalfunctionService
  ) { }

  getd(id) {
    return this.apartmentmalfunctionService.getById(id);
  }

  getu(id) {
    return this.apiService.get(this.configUrlService.get_user_by_id + id);
  }
}
