import { Component, OnInit } from '@angular/core';
import { DetailincidentService } from './detailincident.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-incident',
  templateUrl: './detail-incident.component.html',
  styleUrls: ['./detail-incident.component.css']
})
export class DetailIncidentComponent implements OnInit {
  public detail;
  public employee;
  constructor(
    private detailincidentService: DetailincidentService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getDetailIncident();
  }

  getDetailIncident() {
    const id = this.route.snapshot.paramMap.get('id');
    this.detailincidentService.getd(id).subscribe(data => {
      this.detail = data;
      if (this.detail.employeeid === -1) {
        return;
      } else {
        this.detailincidentService.getu(this.detail.employeeid).subscribe(empl => {
          this.employee = empl;
        });
      }
    });
  }

}
