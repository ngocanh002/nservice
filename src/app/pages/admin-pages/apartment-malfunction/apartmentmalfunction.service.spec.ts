import { TestBed } from '@angular/core/testing';

import { ApartmentmalfunctionService } from './apartmentmalfunction.service';

describe('ApartmentmalfunctionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApartmentmalfunctionService = TestBed.get(ApartmentmalfunctionService);
    expect(service).toBeTruthy();
  });
});
