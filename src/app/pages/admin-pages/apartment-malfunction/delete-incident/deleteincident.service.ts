import { Injectable } from '@angular/core';
import { ApiService } from '../../../../service/api.service';
import { ConfigUrlService } from '../../../../service/config-url.service';

@Injectable({
  providedIn: 'root'
})
export class DeleteincidentService {

  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  deleteIncident(id) {
    return this.apiService.delete(this.configUrlService.delete_incident + id);
  }
}
