import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteincidentService } from './deleteincident.service';

@Component({
  selector: 'app-delete-incident',
  templateUrl: './delete-incident.component.html',
  styleUrls: ['./delete-incident.component.css']
})
export class DeleteIncidentComponent implements OnInit {

  constructor(
    private deleteincidentService: DeleteincidentService,
    public dialogRef: MatDialogRef<DeleteIncidentComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { console.log(data); }

  ngOnInit() {
  }

  deleteInciden() {
    this.deleteincidentService.deleteIncident(this.data.id).subscribe((data) => {
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
