import { TestBed } from '@angular/core/testing';

import { DeleteincidentService } from './deleteincident.service';

describe('DeleteincidentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteincidentService = TestBed.get(DeleteincidentService);
    expect(service).toBeTruthy();
  });
});
