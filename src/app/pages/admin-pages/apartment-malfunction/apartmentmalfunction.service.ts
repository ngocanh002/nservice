import { Injectable } from '@angular/core';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApartmentmalfunctionService {
  constructor(
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  getMalunction() {
    return this.apiService.get(this.configUrlService.getAllApartmentMalfunction);
    // .pipe(
    //   catchError(error => this.error = error)
    // );
  }

  getById(id) {
    return this.apiService.get(this.configUrlService.getApartmentMalfunctionById + id);
  }

  getstatus1() {
  // return this.apiService.get(this.configUrlService);
  }
}
