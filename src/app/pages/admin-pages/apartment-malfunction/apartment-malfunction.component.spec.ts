import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentMalfunctionComponent } from './apartment-malfunction.component';

describe('ApartmentMalfunctionComponent', () => {
  let component: ApartmentMalfunctionComponent;
  let fixture: ComponentFixture<ApartmentMalfunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApartmentMalfunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentMalfunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
