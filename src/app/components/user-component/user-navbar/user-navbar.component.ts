import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';
import { Router } from '@angular/router';
import { ApiService } from '../../../service/api.service';
import { ConfigUrlService } from '../../../service/config-url.service';

@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.css']
})
export class UserNavbarComponent implements OnInit {
  public dataNotifications;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { }

  ngOnInit() {
    this.apiService.get(this.configUrlService.get_notification).subscribe(data => {
      this.dataNotifications = data.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.status == 1 || x.status == 2) {
          return x;
        }
      });

    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
