import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';
import { Router } from '@angular/router';
import { ConfigUrlService } from '../../../service/config-url.service';
import { ApiService } from '../../../service/api.service';

@Component({
  selector: 'app-employee-navbar',
  templateUrl: './employee-navbar.component.html',
  styleUrls: ['./employee-navbar.component.css']
})
export class EmployeeNavbarComponent implements OnInit {
  public currentUser;
  public dataNotifications;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private apiService: ApiService,
    private configUrlService: ConfigUrlService
  ) { this.currentUser = this.authenticationService.currentUserValue.lastname; }

  ngOnInit() {
    this.apiService.get(this.configUrlService.get_notification).subscribe(data => {
      this.dataNotifications = data.filter(x => {
        // tslint:disable-next-line:triple-equals
        if (x.status == 1 || x.status == 3) {
          return x;
        }
      });

    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
