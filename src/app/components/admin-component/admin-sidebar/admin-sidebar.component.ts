import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../service/authentication.service';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.css']
})
export class AdminSidebarComponent implements OnInit {
  public name;
  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(data => this.name = data);
  }

  ngOnInit() {
  }

}
