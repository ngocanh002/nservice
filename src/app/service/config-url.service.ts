import { Injectable } from '@angular/core';

@Injectable()
export class ConfigUrlService {
    private BASE_URL_API_VS = '/v1';
    private BASE_URL_API_TOKEN = '/api';

    // tslint:disable-next-line: variable-name
    // private _get_user = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/user';
    // auth
    // tslint:disable-next-line: variable-name
    private _auth_login = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/users/authenticate';
    // get all resident
    // tslint:disable-next-line:variable-name
    private _get_all_user = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/users?role=2';

    // get info apartment malfunction
    // tslint:disable-next-line: variable-name
    private _get_all_apartment_malfunction = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/apartment-malfunction';

    // tslint:disable-next-line: variable-name
    private _get_apartment_malfunction_by_id = '/apartment-malfunction/';

    // tslint:disable-next-line: variable-name
    private _get_all_employee = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/users?role=3';


    // tslint:disable-next-line: variable-name
    private _post_apartment_malfunction = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/apartment-malfunction';

    // tslint:disable-next-line: variable-name
    private _get_while_list = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/while-list';

    // tslint:disable-next-line: variable-name
    private _put_apartment_malfunction_employee_id = '/apartment-malfunction/';

    // tslint:disable-next-line: variable-name
    private _get_detail_notification = '/apartment-malfunction/';

    // tslint:disable-next-line: variable-name
    private _get_list_incident = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/apartment-malfunction/?status=3';

    // tslint:disable-next-line: variable-name
    private _post_feedback = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/feedback';

    // tslint:disable-next-line:variable-name
    private _get_feedback = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/feedback';

    // tslint:disable-next-line: variable-name
    private _create_new_resident = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/users';

    // tslint:disable-next-line: variable-name
    private _create_new_employee = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/users';

    // tslint:disable-next-line: variable-name
    private _get_user_by_id = '/users/';

    // tslint:disable-next-line:variable-name
    private _put_user = '/users/';

    // tslint:disable-next-line: variable-name
    private _delete_employee = '/users/';

    // tslint:disable-next-line: variable-name
    private _delete_inciden = '/apartment-malfunction/';

    // tslint:disable-next-line:variable-name
    private _get_feedback_by_id = '/feedback/';

    // tslint:disable-next-line:variable-name
    private _post_notification = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/notification';

    // tslint:disable-next-line: variable-name
    private _get_notification = this.BASE_URL_API_TOKEN + this.BASE_URL_API_VS + '/notification?_sort=id,views&_order=desc';

    // tslint:disable-next-line:variable-name
    private _edit_notification = '/notification/';

    // tslint:disable-next-line: variable-name
    private _get_notication_by_id = '/notification/';

    get auth(): string {
        return this._auth_login;
    }

    get getAllUser(): string {
        return this._get_all_user;
    }

    get getEmployee(): string {
        return this._get_all_employee;
    }

    get getAllApartmentMalfunction(): string {
        return this._get_all_apartment_malfunction;
    }

    get getApartmentMalfunctionById(): string {
        return this._get_apartment_malfunction_by_id;
    }

    get postApartmentMalfunction(): string {
        return this._post_apartment_malfunction;
    }

    get getWhileList(): string {
        return this._get_while_list;
    }

    get putNotification(): string {
        return this._put_apartment_malfunction_employee_id;
    }

    get getDetailNotification(): string {
        return this._get_detail_notification;
    }

    get getListIncident(): string {
        return this._get_list_incident;
    }

    get postFeedback(): string {
        return this._post_feedback;
    }

    get getFeedback(): string {
        return this._get_feedback;
    }

    get postResident(): string {
        return this._create_new_resident;
    }

    get postEmployee(): string {
        return this._create_new_employee;
    }

    get get_user_by_id(): string {
        return this._get_user_by_id;
    }

    get put_user(): string {
        return this._put_user;
    }

    get delete_employee(): string {
        return this._delete_employee;
    }

    get delete_incident(): string {
        return this._delete_inciden;
    }

    get get_feedback_by_id(): string {
        return this._get_feedback_by_id;
    }

    get post_notification(): string {
        return this._post_notification;
    }

    get get_notification(): string {
        return this._get_notification;
    }

    get edit_notification(): string {
        return this._edit_notification;
    }

    get get_notication_by_id(): string {
        return this._get_notication_by_id;
    }
}
