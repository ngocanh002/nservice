// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB5iEhxzYLCOGWasP2hApSJp8IGdfJ7ST4',
    authDomain: 'ngservice-17295.firebaseapp.com',
    databaseURL: 'https://ngservice-17295.firebaseio.com',
    projectId: 'ngservice-17295',
    storageBucket: 'ngservice-17295.appspot.com',
    messagingSenderId: '590668550365',
    appId: '1:590668550365:web:d5b6ba46a1c069a7b78b34',
    measurementId: 'G-S1KQD4M9VM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
